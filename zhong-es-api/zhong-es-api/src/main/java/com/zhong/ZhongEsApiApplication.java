package com.zhong;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZhongEsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZhongEsApiApplication.class, args);
	}

}
