package com.zhong;

import com.alibaba.fastjson.JSON;
import com.zhong.pojo.User;
import org.apache.lucene.util.QueryBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import javax.naming.directory.SearchResult;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


/**
 * es 7.6.1 api测试
 */
@SpringBootTest
class ZhongEsApiApplicationTests {

	// 面向对象来操作
	@Autowired
	@Qualifier("restHighLevelClient")
	private RestHighLevelClient client;

	// 测试索引的创建 Request
	@Test
	void testCreateIndex() throws IOException {
		// 1、创建索引的请求
		CreateIndexRequest request = new CreateIndexRequest("zhong_index1");
		// 2、执行创建的请求 indicesclient,请求后获得响应
		CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
		System.out.println(createIndexResponse);
	}
	// 测试获取索引,只能判断存不存在
	@Test
	void testExistIndex() throws IOException {
		GetIndexRequest request = new GetIndexRequest("zhong_index");
		boolean exists = client.indices().exists(request, RequestOptions.DEFAULT);
		System.out.println(exists);
	}

	// 测试删除索引
	@Test
	void testDeleteIndex() throws IOException {
		DeleteIndexRequest request = new DeleteIndexRequest("zhong_index1");
		AcknowledgedResponse delete = client.indices().delete(request, RequestOptions.DEFAULT);
		System.out.println(delete.isAcknowledged());
	}

	// 测试添加文档
	@Test
	void testAddDocument() throws IOException {
		// 创建对象
		User user = new User("星迴", 18);
		// 创建请求
		IndexRequest request = new IndexRequest("xing_index");
		// 设置规则
		request.id("1");
		request.timeout(TimeValue.timeValueSeconds(1));
		request.timeout("1s");
		// 将我们的数据放入请求  json
		request.source(JSON.toJSONString(user), XContentType.JSON);
		// 客户端发送请求, 获取响应的结果
		IndexResponse index = client.index(request, RequestOptions.DEFAULT);
		System.out.println(index.toString()); //
		System.out.println(index.status());  // 对应返回状态CREATE
	}

	// 获取文档，判断是否存在
	@Test
	void testExists() throws IOException {
		GetRequest getRequest = new GetRequest("xing_index", "1");
		// 不获取返回的_source 的上下文了
		getRequest.fetchSourceContext(new FetchSourceContext(false));
		getRequest.storedFields("_none");

		boolean exists = client.exists(getRequest, RequestOptions.DEFAULT);
		System.out.println(exists);
	}
	// 获取文档的信息
	@Test
	void testGetDocument() throws IOException {
		GetRequest getRequest = new GetRequest("xing_index", "1");
		GetResponse getResponse = client.get(getRequest, RequestOptions.DEFAULT);
		System.out.println(getResponse.getSourceAsString()); // 打印文档的内容
		System.out.println(getResponse);  // 返回的全部内容和命令是一样的
	}

	// 更新文档内容
	@Test
	void testUpdateDocument() throws IOException {
		UpdateRequest updateRequest = new UpdateRequest("xing_index", "1");
		updateRequest.timeout("1s");

		User user = new User("java", 12);
		updateRequest.doc(JSON.toJSONString(user),XContentType.JSON);

		UpdateResponse update = client.update(updateRequest, RequestOptions.DEFAULT);
		System.out.println(update.status());
	}

	// 删除文档
	@Test
	void testDeleteDocument() throws IOException {
		DeleteRequest deleteRequest = new DeleteRequest("xing_index", "1");
		deleteRequest.timeout("1s"); // 请求超时不执行

		DeleteResponse delete = client.delete(deleteRequest, RequestOptions.DEFAULT);
		System.out.println(delete.status());

	}

	//  特殊的，真实项目一般会批量插入数据
	@Test
	void testBulkRequest() throws IOException {
		BulkRequest bulkRequest = new BulkRequest();
		bulkRequest.timeout("10s");
		ArrayList<User> users = new ArrayList<>();
		users.add(new User("星迴",18));
		users.add(new User("星迴1",18));
		users.add(new User("星迴2",18));
		users.add(new User("星迴3",18));
		users.add(new User("星迴4",18));
		users.add(new User("星迴5",18));
		// 批处理请求
		for (int i = 0; i < users.size(); i++) {
			bulkRequest.add(
					new IndexRequest("xing_index")
					.id(""+(i+1))
					.source(JSON.toJSONString(users.get(i)),XContentType.JSON));
		}
		BulkResponse bulk = client.bulk(bulkRequest, RequestOptions.DEFAULT);
		System.out.println(bulk.hasFailures());
	}

	// 查询
	// SearchRequest 搜索请求
	// SearchSourceBuilder 条件构造
	//
	@Test
	void testSearch() throws IOException {
		SearchRequest searchRequest = new SearchRequest("xing_index");
		// 构建搜索条件
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		// 查询条件，使用QueryBuilders快速匹配
		// termQuery 精确匹配
		// matchAllQuery 匹配所有
		TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name.keyword", "星迴");
//		MatchAllQueryBuilder matchAllQueryBuilder = QueryBuilders.matchAllQuery();
		searchSourceBuilder.query(termQueryBuilder);
//		searchSourceBuilder.from();  // 分页
//		searchSourceBuilder.size();
		searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
		searchRequest.source(searchSourceBuilder);

		SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
		System.out.println(JSON.toJSONString(search.getHits()));
		System.out.println("===========");
		for (SearchHit hit : search.getHits().getHits()) {
			System.out.println(hit.getSourceAsMap());
		}
	}

}
