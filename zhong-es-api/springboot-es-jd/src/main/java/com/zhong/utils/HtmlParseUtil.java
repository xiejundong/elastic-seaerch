package com.zhong.utils;

import com.zhong.pojo.Content;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Component
public class HtmlParseUtil {
//    public static void main(String[] args) throws IOException {
//        new HtmlParseUtil().parseJD("码出高效").forEach(System.out::println);
//
//    }

    public List<Content> parseJD(String keyword) throws IOException {
        // 获取请求  要联网 https://search.jd.com/Search?keyword=Java
        // ajax 不能获取得到
        String url = "https://search.jd.com/Search?keyword=" + keyword + "&enc=utf-8";
        // 解析网页 (Jsoup返回的document对象，就是浏览器document对象)
        Document document = Jsoup.parse(new URL(url), 30000);
        // 所有在js中可以用的方法，这里都可以用
        Element element = document.getElementById("J_goodsList");
        // 获取所有的li元素
        Elements elements = element.getElementsByTag("li");


        ArrayList<Content> goodsList = new ArrayList<>();
        // 获取元素中的内容
        for (Element el : elements) {
            // 关于这种图片特别多的网站，所有的图片都是延迟加载的（懒加载）
            // source-data-lazy-img
            String img = el.getElementsByTag("img").eq(0).attr("data-lazy-img");
            String price = el.getElementsByClass("p-price").eq(0).text();
            String title = el.getElementsByClass("p-name").eq(0).text();

            Content content = new Content();
            content.setImg(img);
            content.setPrice(price);
            content.setTitle(title);

            goodsList.add(content);
        }
        return goodsList;
    }
}
